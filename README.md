# Croissant app

Spring Boot MVC application with REST API written in Kotlin.

### Requirements
- Java 11
- Docker

### How to run
Build
```
./gradlew clean build
```

Or for full output:
```
./gradlew clean build -i
```

Run
```
docker-compose up --build
```

It's needed to build an app first before run.

### API doc
Available via [SpringDoc endpoint](http://localhost:8080/swagger-ui/index.html).

### Order flow

It's possible to create a `product`. It's possible to delete it, but it's done as change of state because of possible foreign keys (I don't check if there are any).

I didn't implement authentication, but in API there is `userId` parameter which I use instead of something like `sessionId`.

User can have only one `cart` at the time being. If `cart` order is not finished within 30 minutes, cart is discarded.

It's possible to create `cart items`. When there is not existing `cart` a new `cart` is created, otherwise `cart item` is added on the existing `cart`.

It's possible to create an `order`. When `order` is submitted all items in current cart are discarded and moved into `order`.

`Order` can be cancelled. Implemented as state change.

`Order` can be payed. It's implemented as simple change of `order` state.

Changes which affect `products` quantity should be propagated eg. change of `cart item` quantity, deletion of `item cart` (also from cart deletion), order cancellation.

## Implementation details
First startup can take longer because it's needed to download images.

I decided not to use in-memory database because it's better to test directly with the database used in production (if possible).

I didn't create "production" application properties. Configuration is for development only.

I am not handling database evolution.

I am not solving changes of prices, currencies, more stocks etc.

Expiration of carts is handled naively in case of multiple instancies deployment.

This [article](https://medium.com/swlh/defining-jpa-hibernate-entities-in-kotlin-1ff8ee470805) was useful source of information about using entities in Kotlin. 

I didn't split tests into separated source sets.
