package com.matejuh.croissant

import org.springframework.http.ResponseEntity
import org.springframework.web.util.UriTemplate

inline fun <T, reified R : ApiObject> createdResponse(
    uriTemplate: String,
    factory: ApiObjectFactory<T, R>,
    createDto: () -> T
): ResponseEntity<R> =
    createDto().let {
        val apiObject = factory.fromDto(it)
        ResponseEntity
            .created(UriTemplate(uriTemplate).expand(apiObject.id))
            .body(apiObject)
    }

interface ApiObject {
    val id: String
}

interface ApiObjectFactory<in T, out R : ApiObject> {
    fun fromDto(dto: T): R
}
