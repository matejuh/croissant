package com.matejuh.croissant

import io.swagger.v3.oas.annotations.media.Schema
import org.springdoc.api.ErrorMessage
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.context.request.ServletWebRequest
import java.time.Instant
import javax.validation.ConstraintViolationException
import javax.validation.ValidationException


@ResponseStatus(HttpStatus.NOT_FOUND)
class EntityNotFoundException(message: String) : RuntimeException(message, null, false, false)

// mimic default Spring Boot exception handling message
data class ApiError(
    @field:Schema(required = true, description = "Error http status", example = "500")
    val status: Int,
    @field:Schema(required = true, description = "Error message", example = "Server error")
    val error: String,
    @field:Schema(
        required = true,
        description = "Path where error happened",
        example = "/products/2801206c-502b-48fd-91f0-589a6883fe2a"
    )
    val path: String,
    @field:Schema(required = true, description = "Timestamp when error happened id", example = "1642956184477")
    val timestamp: Long = Instant.now().epochSecond
)


@ControllerAdvice
class ControllerExceptionHandler {
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(
        value = [ValidationException::class, ConstraintViolationException::class]
    )
    fun exceptionHandler(e: ValidationException, request: ServletWebRequest): ApiError =
        ApiError(
            HttpStatus.BAD_REQUEST.value(),
            e.message ?: "",
            request.request.requestURI
        )
}
