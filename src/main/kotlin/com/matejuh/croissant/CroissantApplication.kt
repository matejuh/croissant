package com.matejuh.croissant

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class CroissantApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
	runApplication<CroissantApplication>(*args)
}
