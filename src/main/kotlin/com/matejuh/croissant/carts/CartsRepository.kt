package com.matejuh.croissant.carts

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.time.Instant

interface CartsRepository: CrudRepository<CartDto, String> {
    fun findByUserId(userId: String): CartDto?
    fun findByCreatedDateBefore(expirationDate: Instant): Iterable<CartDto>
    @Modifying
    @Query(value = "DELETE from cart c WHERE c.createdDate < :expirationDate")
    fun removeExpired(@Param("expirationDate") expirationDate: Instant): Int
}
