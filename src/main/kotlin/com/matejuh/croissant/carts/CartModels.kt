package com.matejuh.croissant.carts

import com.matejuh.croissant.ApiObject
import com.matejuh.croissant.ApiObjectFactory
import com.matejuh.croissant.orders.UserId
import com.matejuh.croissant.products.ProductId
import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.Instant
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

const val MAX_ITEMS = 100L

@Entity(name = "cart")
@Table(name = "carts")
open class CartDto(
    @field:Column(updatable = false, unique = true)
    @field:NotNull
    open var userId: String,
    @field:Column(updatable = false)
    open var createdDate: Instant = Instant.now(),
    @field:Id
    @field:GeneratedValue(generator = "UUID")
    @field:GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
    )
    @field:Column(updatable = false)
    open var id: String? = null
) {
    @field:OnDelete(action = OnDeleteAction.CASCADE)
    @field:OneToMany(mappedBy = "cart", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    open var items: MutableList<CartItemDto> = mutableListOf()
}

@Entity(name = "cart_item")
@Table(name = "carts_items")
open class CartItemDto(
    @field:Column(updatable = false)
    @field:NotNull
    open var productId: String,
    @field:ManyToOne(fetch = FetchType.LAZY, optional = false)
    @field:JoinColumn(name = "cart_id", nullable = false, updatable = false)
    open var cart: CartDto,
    @field:Min(0)
    @field:Max(MAX_ITEMS)
    open var quantity: Int,
    @field:Id
    @field:GeneratedValue(generator = "UUID")
    @field:GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
    )
    @field:Column(updatable = false)
    open var id: String? = null
)

data class CreateCartItem(
    @field:Schema(required = true, description = "Product id", example = "2801206c-502b-48fd-91f0-589a6883fe2a")
    val productId: ProductId,
    @field:Schema(
        description = "Id of user for whom should be cart created",
        required = true,
        example = "2801206c-502b-48fd-91f0-589a6883fe2a"
    )
    val userId: UserId,
    @field:Schema(
        description = "How many units of product should be in the cart",
        required = true,
        example = "1"
    )
    val quantity: Int
)

data class UpdateCartItemQuantity(
    @field:Schema(
        description = "How many units of product should be in the cart",
        required = true,
        example = "1",
        minimum = "0",
        maximum = "$MAX_ITEMS"
    )
    @field:Min(0)
    @field:Max(MAX_ITEMS)
    val quantity: Int
)

data class CartItem(
    @field:Schema(required = true, description = "Cart item id", example = "8fee725b-f94b-4bc1-a610-b8329c6da5b4")
    @field:NotBlank
    override val id: CartItemId,
    @field:Schema(required = true, description = "Product id", example = "2801206c-502b-48fd-91f0-589a6883fe2a")
    @field:NotBlank
    val productId: ProductId,
    @field:Schema(
        description = "How many units of product should are in the cart",
        required = true,
        example = "1",
        minimum = "0",
        maximum = "$MAX_ITEMS"
    )
    @field:Min(0)
    @field:Max(MAX_ITEMS)
    val quantity: Int
) : ApiObject {
    companion object : ApiObjectFactory<CartItemDto, CartItem> {
        override fun fromDto(dto: CartItemDto): CartItem =
            CartItem(dto.id!!, dto.productId, dto.quantity)
    }
}

typealias CartItemId = String
