package com.matejuh.croissant.carts

import com.matejuh.croissant.products.ProductsRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.repository.findByIdOrNull
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.Instant
import java.util.concurrent.TimeUnit
import javax.persistence.EntityNotFoundException

private val logger = KotlinLogging.logger {}

@Service
class CartsService(
    private val cartsRepository: CartsRepository,
    private val cartsItemsRepository: CartsItemsRepository,
    private val productsRepository: ProductsRepository,
    @Value("\${carts.expiration.scheduler.enabled:true}") private val expirationSchedulerEnabled: Boolean,
    @Value("\${carts.expiration:30m}") private val cartsExpiration: Duration
) {
    /**
     * Create cart item
     * Throws an exception when product quantity would be less than 0
     */
    @Transactional
    fun createCartItem(create: CreateCartItem): CartItemDto {
        val userId = create.userId
        val cart = cartsRepository.findByUserId(userId) ?: cartsRepository.save(CartDto(userId))
        productsRepository.changeQuantity(create.productId, -1 * create.quantity)
        return cartsItemsRepository.save(create.toDto(cart))
    }

    /**
     * Update cart item quantity
     * If cart item quantity set to 0, delete cart item
     * Returns updated cart item or null when quantity update was to 0
     * Throws an exception when product quantity would be less than 0
     */
    @Transactional
    fun updateCartItem(
        cartItemId: String,
        update: UpdateCartItemQuantity
    ): CartItemDto? {
        val cartItem = cartsItemsRepository.findByIdOrNull(cartItemId)
            ?: throw EntityNotFoundException("Cart item with id $cartItemId was not found!")
        val quantityChange = cartItem.quantity - update.quantity
        productsRepository.changeQuantity(cartItem.productId, quantityChange)
        return if (update.quantity == 0) {
            cartsItemsRepository.delete(cartItem)
            null
        } else {
            cartItem.quantity = update.quantity
            cartsItemsRepository.save(cartItem)
        }
    }

    /**
     * Discard evicted carts
     */
    @Scheduled(fixedRateString = "\${carts.expiration.scheduler.rate}", timeUnit = TimeUnit.SECONDS)
    @Transactional
    fun evictCarts() {
        logger.debug { "action=evict_carts" }
        if (expirationSchedulerEnabled) {
            removeExpiredCarts()
        }
    }

    private fun removeExpiredCarts() {
        val removeBefore = Instant.now().minus(cartsExpiration)
        val expiredCarts = cartsRepository.findByCreatedDateBefore(removeBefore)
        if (expiredCarts.count() > 0) {
            expiredCarts.forEach { cart ->
                cart.items.forEach {
                    productsRepository.changeQuantity(it.productId, it.quantity)
                }
            }
            val removedCount = cartsRepository.removeExpired(removeBefore)
            if (logger.isTraceEnabled) {
                logger.trace { "action=carts_evicted count=$removedCount carts=${expiredCarts.map { it.id }}" }
            } else {
                logger.info { "action=carts_evicted count=$removedCount" }
            }
        }
    }

    private fun CreateCartItem.toDto(cart: CartDto): CartItemDto =
        CartItemDto(productId, cart, quantity)
}
