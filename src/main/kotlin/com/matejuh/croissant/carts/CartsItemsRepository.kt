package com.matejuh.croissant.carts

import org.springframework.data.repository.CrudRepository

interface CartsItemsRepository: CrudRepository<CartItemDto, String> {
    fun countCartItemsByCart(cart: CartDto): Long
}
