package com.matejuh.croissant.carts

import com.matejuh.croissant.ApiError
import com.matejuh.croissant.createdResponse
import com.matejuh.croissant.products.Product
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.headers.Header
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Tag(name = "Carts", description = "Carts and carts items manipulation")
@RestController
@RequestMapping(CartsController.BASE_URI)
@Validated
class CartsController(private val cartsService: CartsService) {

    companion object {
        const val BASE_URI = "/carts"
        private const val ID = "{id}"
        const val ITEM_URI = "$BASE_URI/$ID"
    }

    @Operation(
        summary = "Create cart items",
        responses = [
            ApiResponse(
                responseCode = "201",
                description = "Cart item created",
                headers = [
                    Header(
                        name = "Location",
                        description = "Created cart item path",
                    )
                ],
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = CartItem::class)
                    )
                ]
            )
        ]
    )
    @PostMapping
    fun createCartItem(@Valid @RequestBody create: CreateCartItem): ResponseEntity<CartItem> =
        createdResponse(ITEM_URI, CartItem) { cartsService.createCartItem(create) }

    @Operation(
        summary = "Update cart item quantity",
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "Cart item quantity updated",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = CartItem::class)
                    )
                ]
            ),
            ApiResponse(
                responseCode = "204",
                description = "Cart item removed because quantity set to 0"
            ),
            ApiResponse(
                responseCode = "404",
                description = "Cart item not found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @PutMapping("/$ID")
    fun updateQuantity(
        @Parameter(description = "Cart item id", example = "8fee725b-f94b-4bc1-a610-b8329c6da5b4")
        @PathVariable id: String,
        @Valid @RequestBody updateQuantity: UpdateCartItemQuantity
    ): ResponseEntity<CartItem> =
        cartsService.updateCartItem(id, updateQuantity)
            ?.let {
                ResponseEntity.ok().body(CartItem.fromDto(it))
            } ?: ResponseEntity.noContent().build()
}
