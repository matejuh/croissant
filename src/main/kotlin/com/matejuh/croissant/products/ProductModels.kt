package com.matejuh.croissant.products

import com.fasterxml.jackson.annotation.JsonInclude
import com.matejuh.croissant.ApiObject
import com.matejuh.croissant.ApiObjectFactory
import com.matejuh.croissant.carts.MAX_ITEMS
import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.GenericGenerator
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity(name = "product")
@Table(name = "products")
@JsonInclude(JsonInclude.Include.NON_NULL)
open class ProductDto(
    @field:Column(updatable = false)
    @field:NotEmpty
    open var name: String,
    @field:Min(0)
    open var price: Double,
    @field:Column(updatable = false)
    @field:NotEmpty
    open var unit: String,
    @field:Column(columnDefinition = "INTEGER CHECK (quantity >= 0)")
    @field:Min(0)
    open var quantity: Int,
    @field:Enumerated(EnumType.STRING)
    open var state: ProductState = ProductState.ACTIVE,
    @field:Id
    @field:GeneratedValue(generator = "UUID")
    @field:GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
    )
    @field:Column(updatable = false)
    open var id: String? = null
)

enum class ProductState {
    ACTIVE, DELETED
}

data class ProductCreate(
    @field:Schema(required = true, example = "milk")
    @field:NotBlank
    val name: String,
    @field:Schema(required = true, example = "30.0", minimum = "0.0")
    @field:Min(0)
    val price: Double,
    @field:Schema(required = true, example = "bottle")
    @field:NotBlank
    val unit: String,
    @field:Schema(required = true, example = "2", minimum = "0")
    @field:Min(0)
    val quantity: Int
) {
    fun toDto(): ProductDto =
        ProductDto(
            name,
            price,
            unit,
            quantity
        )
}

data class Product(
    @field:Schema(required = true, example = "milk")
    @field:NotBlank
    val name: String,
    @field:Schema(required = true, example = "30.0", minimum = "0.0")
    @field:Min(0)
    val price: Double,
    @field:Schema(required = true, example = "bottle")
    @field:NotBlank
    val unit: String,
    @field:Schema(required = true, example = "2", minimum = "0")
    @field:Min(0)
    val quantity: Int,
    @field:Schema(required = true, description = "Product id", example = "2801206c-502b-48fd-91f0-589a6883fe2a")
    @field:NotNull
    override val id: ProductId
) : ApiObject {

    companion object : ApiObjectFactory<ProductDto, Product> {
        override fun fromDto(dto: ProductDto): Product = Product(
            dto.name,
            dto.price,
            dto.unit,
            dto.quantity,
            dto.id!!
        )
    }
}

typealias ProductId = String

