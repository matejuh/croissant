package com.matejuh.croissant.products

import com.matejuh.croissant.EntityNotFoundException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ProductsService(private val productsRepository: ProductsRepository) {
    /**
     * Create a new product
     */
    fun create(product: ProductCreate): ProductDto =
        productsRepository.save(product.toDto())

    /**
     * Get product by its id or null
     */
    fun get(id: ProductId): ProductDto? =
        productsRepository.findByIdOrNull(id)

    /**
     * Delete product
     * @throws EntityNotFoundException when product does not exist
     */
    @Transactional
    fun delete(id: ProductId): ProductDto? =
        get(id)?.let {
            it.state = ProductState.DELETED
            productsRepository.save(it)
        } ?: throw EntityNotFoundException("Product with id: $id was not found!")
}
