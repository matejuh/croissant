package com.matejuh.croissant.products

import com.matejuh.croissant.ApiError
import com.matejuh.croissant.EntityNotFoundException
import com.matejuh.croissant.createdResponse
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.headers.Header
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Tag(name = "Products", description = "Products manipulation")
@RestController
@RequestMapping(ProductsController.BASE_URI)
@Validated
class ProductsController(private val productService: ProductsService) {

    companion object {
        const val BASE_URI = "/products"
        private const val ID = "{id}"
        const val ITEM_URI = "$BASE_URI/$ID"
    }

    @Operation(
        summary = "Create product",
        responses = [
            ApiResponse(
                responseCode = "201",
                description = "Product created",
                headers = [
                    Header(
                        name = "Location",
                        description = "Created product path",
                    )
                ],
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = Product::class)
                    )
                ]
            )
        ]
    )
    @PostMapping
    fun create(@Valid @RequestBody product: ProductCreate): ResponseEntity<Product> =
        createdResponse(ITEM_URI, Product) { productService.create(product) }

    @Operation(
        summary = "Get product",
        description = "Get product by it's id",
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "Product found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = Product::class)
                    )
                ]
            ),
            ApiResponse(
                responseCode = "404",
                description = "Product not found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @GetMapping("/$ID")
    fun get(
        @Parameter(description = "Product id", example = "2801206c-502b-48fd-91f0-589a6883fe2a")
        @PathVariable id: String
    ): Product =
        productService.get(id)?.let { Product.fromDto(it) }
            ?: throw EntityNotFoundException("Product with id $id was not found")

    @Operation(
        summary = "Delete product",
        description = "Delete product by it's id",
        responses = [
            ApiResponse(
                responseCode = "204",
                description = "Product deleted"
            ),
            ApiResponse(
                responseCode = "404",
                description = "Product not found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @DeleteMapping("/$ID")
    fun delete(
        @Parameter(description = "Product id", example = "2801206c-502b-48fd-91f0-589a6883fe2a")
        @PathVariable id: String
    ): ResponseEntity<Void> {
        productService.delete(id)
        return ResponseEntity.noContent().build()
    }
}
