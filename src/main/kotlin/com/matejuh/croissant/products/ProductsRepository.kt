package com.matejuh.croissant.products

import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param

interface ProductsRepository : CrudRepository<ProductDto, String> {

    @Modifying
    @Query(value = "UPDATE product p SET p.quantity=p.quantity + :variance WHERE p.id=:id")
    fun changeQuantity(@Param("id") id: String, @Param("variance") change: Int): Int
}
