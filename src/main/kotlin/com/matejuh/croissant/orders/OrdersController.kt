package com.matejuh.croissant.orders

import com.matejuh.croissant.ApiError
import com.matejuh.croissant.createdResponse
import com.matejuh.croissant.products.Product
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.headers.Header
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Tag(name = "Orders", description = "Orders manipulation")
@RestController
@RequestMapping(OrdersController.BASE_URI)
@Validated
class OrdersController(private val ordersService: OrdersService) {
    companion object {
        const val BASE_URI = "/orders"
        private const val ID = "{id}"
        const val ITEM_URI = "$BASE_URI/$ID"
    }

    @Operation(
        summary = "Create order",
        description = "Submit current user cart as order",
        responses = [
            ApiResponse(
                responseCode = "201",
                description = "Order created",
                headers = [
                    Header(
                        name = "Location",
                        description = "Created order path",
                    )
                ],
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = Order::class)
                    )
                ]
            ),
            ApiResponse(
                responseCode = "404",
                description = "There is no cart for which can be order created",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @PostMapping
    fun createOrder(@Valid @RequestBody orderSubmission: OrderSubmission): ResponseEntity<Order> =
        createdResponse(ITEM_URI, Order) { ordersService.submitOrder(orderSubmission) }

    @Operation(
        summary = "Cancel order",
        description = "Cancel order by it's id",
        responses = [
            ApiResponse(
                responseCode = "204",
                description = "Order cancelled"
            ),
            ApiResponse(
                responseCode = "404",
                description = "Order not found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Order in state which can't be cancelled",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @DeleteMapping("/$ID")
    fun cancelOrder(@PathVariable id: String): ResponseEntity<Void> {
        ordersService.cancelOrder(id)
        return ResponseEntity.noContent().build()
    }

    @Operation(
        summary = "Confirm payment",
        description = "Confirm order payment",
        responses = [
            ApiResponse(
                responseCode = "204",
                description = "Order set as payed"
            ),
            ApiResponse(
                responseCode = "404",
                description = "Order not found",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            ),
            ApiResponse(
                responseCode = "400",
                description = "Order in state which can't be payed",
                content = [
                    Content(
                        mediaType = "application/json",
                        schema = Schema(implementation = ApiError::class)
                    )
                ]
            )
        ]
    )
    @PutMapping("/$ID/payment")
    fun confirmPayment(
        @PathVariable id: String,
        @Valid @RequestBody payment: OrderPayment
    ): ResponseEntity<Void> {
        ordersService.confirmPayment(id, payment)
        return ResponseEntity.noContent().build()
    }
}
