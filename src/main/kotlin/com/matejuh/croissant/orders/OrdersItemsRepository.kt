package com.matejuh.croissant.orders

import org.springframework.data.repository.CrudRepository

interface OrdersItemsRepository: CrudRepository<OrderItemDto, String>
