package com.matejuh.croissant.orders

import com.matejuh.croissant.ApiObject
import com.matejuh.croissant.ApiObjectFactory
import com.matejuh.croissant.carts.MAX_ITEMS
import io.swagger.v3.oas.annotations.media.Schema
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import java.time.Instant
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity(name = "order")
@Table(name = "orders")
open class OrderDto(
    @field:Column(updatable = false)
    @field:NotNull
    open var userId: String,
    @field:Enumerated(EnumType.STRING)
    @field:NotNull
    open var state: OrderState = OrderState.CREATED,
    @field:Column(updatable = false)
    open var createdAt: Instant = Instant.now(),
    @field:Id
    @field:GeneratedValue(generator = "UUID")
    @field:GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
    )
    @field:Column(updatable = false)
    open var id: String? = null,
    open var paymentId: String? = null
) {
    @field:OnDelete(action = OnDeleteAction.CASCADE)
    @field:OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = [CascadeType.REMOVE])
    open var items: MutableSet<OrderItemDto> = mutableSetOf()
}

@Entity(name = "order_item")
@Table(name = "orders_items")
open class OrderItemDto(
    @field:ManyToOne(fetch = FetchType.LAZY, optional = false)
    @field:JoinColumn(name = "order_id", nullable = false, updatable = false)
    open var order: OrderDto,
    @field:Column(updatable = false)
    @field:NotNull
    open var productId: String,
    @field:Min(0)
    @field:Max(MAX_ITEMS)
    open var quantity: Int,
    @field:Id
    @field:GeneratedValue(generator = "UUID")
    @field:GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator",
    )
    @field:Column(updatable = false)
    open var id: String? = null
)

enum class OrderState {
    CREATED,
    CANCELLED,
    PAYED
}

data class OrderSubmission(
    @field:Schema(
        description = "Id of user for whom should be order created",
        required = true,
        example = "2801206c-502b-48fd-91f0-589a6883fe2a"
    )
    @field:NotBlank
    val userId: UserId
)

data class OrderPayment(
    @field:Schema(
        description = "Id of successfully processed payment",
        required = true,
        example = "5cb9072e-d02c-408d-b28b-7ad97025567e"
    )
    @field:NotBlank
    val paymentId: PaymentId
)

data class Order(
    @field:Schema(
        description = "Order id",
        required = true,
        example = "f522070d-dd80-4a5a-adf0-da50060b264f"
    )
    @field:NotBlank
    override val id: OrderId
) : ApiObject {
    companion object : ApiObjectFactory<OrderDto, Order> {
        override fun fromDto(dto: OrderDto): Order =
            Order(dto.id!!)
    }
}

typealias OrderId = String
typealias PaymentId = String
typealias UserId = String
