package com.matejuh.croissant.orders

import org.springframework.data.repository.CrudRepository

interface OrdersRepository: CrudRepository<OrderDto, String>
