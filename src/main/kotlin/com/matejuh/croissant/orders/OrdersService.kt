package com.matejuh.croissant.orders

import com.matejuh.croissant.EntityNotFoundException
import com.matejuh.croissant.carts.CartsRepository
import com.matejuh.croissant.products.ProductsRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.ResponseStatus

@Service
class OrdersService(
    private val ordersRepository: OrdersRepository,
    private val ordersItemsRepository: OrdersItemsRepository,
    private val cartsRepository: CartsRepository,
    private val productsRepository: ProductsRepository
) {
    /**
     * Take all items from cart and add them into order
     * @throws EntityNotFoundException when there is no cart for user
     */
    @Transactional
    fun submitOrder(submit: OrderSubmission): OrderDto {
        val userId = submit.userId
        val cart = cartsRepository.findByUserId(userId)
            ?: throw EntityNotFoundException("No cart found for user $userId")
        val order = ordersRepository.save(OrderDto(userId))
        val orderItems = cart.items.map {
            OrderItemDto(order, it.productId, it.quantity)
        }
        ordersItemsRepository.saveAll(orderItems)
        cartsRepository.delete(cart)
        return order
    }

    /**
     * Change products on store quantities and change order state
     * @throws EntityNotFoundException when order does not exist
     * @throws InvalidOrderStateException when order in state which can't be cancelled
     */
    @Transactional
    fun cancelOrder(id: String): OrderDto {
        val order = getOrder(id)
        if (order.state == OrderState.CREATED) {
            order.items.forEach {
                productsRepository.changeQuantity(it.productId, it.quantity)
            }
            order.state = OrderState.CANCELLED
            return ordersRepository.save(order)
        } else {
            throw InvalidOrderStateException("Can't cancel order in state ${order.state}")
        }
    }

    /**
     * Change order state to OrderState.PAYED
     * @throws EntityNotFoundException when order does not exist
     * @throws InvalidOrderStateException when order in state which can't be apyed
     */
    @Transactional
    fun confirmPayment(id: String, payment: OrderPayment): OrderDto {
        val order = getOrder(id)
        if (order.state == OrderState.CREATED) {
            order.state = OrderState.PAYED
            order.paymentId = payment.paymentId
            return ordersRepository.save(order)
        } else {
            throw InvalidOrderStateException("Can't pay order in state ${order.state}")
        }
    }

    /**
     * Get order by it's id
     * @throws EntityNotFoundException when order does not exist
     */
    fun getOrder(id: String): OrderDto =
        ordersRepository.findByIdOrNull(id)
            ?: throw EntityNotFoundException("Order with id $id was not found")
}

@ResponseStatus(HttpStatus.BAD_REQUEST)
class InvalidOrderStateException(message: String) : RuntimeException(message, null, false, false)
