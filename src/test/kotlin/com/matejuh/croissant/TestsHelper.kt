package com.matejuh.croissant

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.matejuh.croissant.carts.CartDto
import com.matejuh.croissant.products.Product
import com.matejuh.croissant.products.ProductCreate
import com.matejuh.croissant.products.ProductDto
import com.matejuh.croissant.products.ProductId
import io.github.serpro69.kfaker.faker

internal const val IGNORE_JSON_STRING = "#{json-unit.any-string}"

internal val objectMapper = jacksonObjectMapper()
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
internal val faker = faker { }

internal fun productJson(
    product: Product,
): String =
    productJson(
        ProductCreate(product.name, product.price, product.unit, product.quantity),
        product.id
    )

internal fun productJson(
    product: ProductCreate,
    id: String = IGNORE_JSON_STRING
): String =
    """{
        "id": "$id",
        "name": "${product.name}",
        "price": ${product.price},
        "unit": "${product.unit}",
        "quantity": ${product.quantity}
    }""".trimIndent()

internal fun productCreateJson(product: ProductCreate = generateProductCreate()): String =
    """{
        "name": "${product.name}",
        "price": ${product.price},
        "unit": "${product.unit}",
        "quantity": ${product.quantity}
    }""".trimIndent()

internal fun generateProduct(
    name: String = faker.food.fruits(),
    price: Double = faker.random.nextDouble(),
    unit: String = faker.food.metricMeasurements(),
    quantity: Int = faker.random.nextInt(1, 100),
    id: ProductId = faker.random.nextUUID()
): Product =
    Product(
        name,
        price,
        unit,
        quantity,
        id
    )

internal fun generateProductCreate(
    name: String = faker.food.fruits(),
    price: Double = faker.random.nextDouble(),
    unit: String = faker.food.metricMeasurements(),
    quantity: Int = faker.random.nextInt(1, 100)
): ProductCreate =
    ProductCreate(
        name,
        price,
        unit,
        quantity
    )

internal fun generateProductDto(
    name: String = faker.food.fruits(),
    price: Double = faker.random.nextDouble(),
    unit: String = faker.food.metricMeasurements(),
    quantity: Int = faker.random.nextInt(1, 100)
): ProductDto =
    ProductDto(
        name,
        price,
        unit,
        quantity
    )
