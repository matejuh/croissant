package com.matejuh.croissant.products

import com.matejuh.croissant.generateProduct
import com.matejuh.croissant.generateProductCreate
import com.matejuh.croissant.objectMapper
import com.matejuh.croissant.productCreateJson
import com.matejuh.croissant.productJson
import net.javacrumbs.jsonunit.JsonAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ProductModelsTests {
    @Test
    internal fun `should serialize product`() {
        val product = generateProduct()
        val json = objectMapper.writeValueAsString(product)
        JsonAssert.assertJsonEquals(productJson(product), json)
    }

    @Test
    internal fun `should deserialize product`() {
        val expected = generateProduct()
        val json = productJson(expected)
        val product = objectMapper.readValue(json, Product::class.java)
        assertThat(product).isEqualTo(expected)
    }

    @Test
    internal fun `should serialize product create`() {
        val product = generateProductCreate()
        val json = objectMapper.writeValueAsString(product)
        JsonAssert.assertJsonEquals(productCreateJson(product), json)
    }

    @Test
    internal fun `should deserialize product create`() {
        val expected = generateProductCreate()
        val json = productCreateJson(expected)
        val product = objectMapper.readValue(json, ProductCreate::class.java)
        assertThat(product).isEqualTo(expected)
    }
}
