package com.matejuh.croissant.products

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.IGNORE_JSON_STRING
import com.matejuh.croissant.faker
import com.matejuh.croissant.generateProductCreate
import com.matejuh.croissant.productCreateJson
import com.matejuh.croissant.productJson
import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.javacrumbs.jsonunit.JsonMatchers
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus

class ProductsApiTests: CroissantApplicationTests() {

    @Test
    internal fun `should create product`() {
        val productCreate = generateProductCreate()
        val productCreateJson = productCreateJson(productCreate)
        val createdProduct = productJson(productCreate, IGNORE_JSON_STRING)

        RestAssured.given()
            .body(productCreateJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(ProductsController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.CREATED.value())
            .header(HttpHeaders.LOCATION, Matchers.matchesRegex("^/products/[a-zA-Z0-9-]+$"))
            .body(JsonMatchers.jsonEquals<String>(createdProduct))
    }

    @Test
    internal fun `should create not create product when bad input`() {
        val productCreate = generateProductCreate(price = -100.0)

        RestAssured.given()
            .body(productCreate)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(ProductsController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.BAD_REQUEST.value())
    }

    @Test
    internal fun `should get existing product`() {
        val product = generateProductCreate()
        val productId = createProduct(product).id
        RestAssured.given()
            .accept(ContentType.JSON)
            .`when`()
            .get(ProductsController.ITEM_URI, productId)
            .then()
            .assertThat()
            .statusCode(HttpStatus.OK.value())
            .body(JsonMatchers.jsonEquals<String>(productJson(product, IGNORE_JSON_STRING)))
    }

    @Test
    internal fun `should not found not existing product`() {
        RestAssured.given()
            .accept(ContentType.JSON)
            .`when`()
            .get(ProductsController.ITEM_URI, faker.random.nextUUID())
            .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
    }

    @Test
    internal fun `should delete product`() {
        val productId = createProduct().id
        RestAssured.given()
            .accept(ContentType.JSON)
            .`when`()
            .delete(ProductsController.ITEM_URI, productId)
            .then()
            .assertThat()
            .statusCode(HttpStatus.NO_CONTENT.value())
    }

    @Test
    internal fun `should return error when deleting not existing`() {
        RestAssured.given()
            .accept(ContentType.JSON)
            .`when`()
            .delete(ProductsController.ITEM_URI, faker.random.nextUUID())
            .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
    }
}
