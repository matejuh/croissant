package com.matejuh.croissant.products

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.generateProductCreate
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class ProductsServiceTests: CroissantApplicationTests() {

    @Autowired
    private lateinit var productsService: ProductsService

    @Test
    internal fun `should delete product`() {
        val createdProduct = productsService.create(generateProductCreate())
        val deletedProduct = productsService.delete(createdProduct.id!!)
        assertThat(deletedProduct?.state).isEqualTo(ProductState.DELETED)
    }
}
