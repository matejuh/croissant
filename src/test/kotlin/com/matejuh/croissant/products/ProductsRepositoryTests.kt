package com.matejuh.croissant.products

import com.matejuh.croissant.faker
import com.matejuh.croissant.generateProductDto
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProductsRepositoryTests {
    @Autowired
    lateinit var repository: ProductsRepository

    @Autowired
    lateinit var entityManager: TestEntityManager

    @Test
    internal fun `should create`() {
        val product = repository.save(generateProductDto())
        assertThat(product).isNotNull
        assertThat(product.id).isNotNull
    }

    @Test
    internal fun `should get`() {
        val id = create()
        assertThat(repository.findByIdOrNull(id)).isNotNull
    }

    @Test
    internal fun `should delete`() {
        val id = create()
        repository.deleteById(id)
        assertThat(repository.findByIdOrNull(id)).isNull()
    }

    @Test
    internal fun `should change positive quantity when exists`() {
        // given
        val quantity = 0
        val variance = 5
        val id = create(generateProductDto(quantity = quantity))

        // when
        val updated = repository.changeQuantity(id, variance)

        // then
        assertThat(updated).isEqualTo(1)
        assertThat(get(id).quantity).isEqualTo(quantity + variance)
    }

    @Test
    internal fun `should change negative quantity when exists`() {
        // given
        val quantity = 10
        val variance = -3
        val id = create(generateProductDto(quantity = quantity))

        // when
        val updated = repository.changeQuantity(id, variance)

        // then
        assertThat(updated).isEqualTo(1)
        assertThat(get(id).quantity).isEqualTo(quantity + variance)
    }

    @Test
    internal fun `should fail when change under 0`() {
        // given
        val quantity = 0
        val variance = -1
        val id = create(generateProductDto(quantity = quantity))

        // when
        val thrown: Throwable = Assertions.catchThrowable { repository.changeQuantity(id, variance) }

        // then
        assertThat(thrown).isInstanceOf(DataIntegrityViolationException::class.java)
    }

    @Test
    internal fun `should not change quantity when not existing`() {
        // when
        val updated = repository.changeQuantity(faker.random.nextUUID(), faker.random.nextInt(10))

        // then
        assertThat(updated).isEqualTo(0)
    }

    private fun create(product: ProductDto = generateProductDto()): String =
        entityManager.persistAndFlush(product).let { it.id!! }

    private fun get(id: String): ProductDto {
        entityManager.flush()
        entityManager.clear()
        return entityManager.find(ProductDto::class.java, id)
    }
}
