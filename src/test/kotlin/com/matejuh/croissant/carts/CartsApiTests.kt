package com.matejuh.croissant.carts

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.IGNORE_JSON_STRING
import com.matejuh.croissant.faker
import com.matejuh.croissant.generateProductCreate
import com.matejuh.croissant.products.ProductId
import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.javacrumbs.jsonunit.JsonMatchers
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus

class CartsApiTests : CroissantApplicationTests() {
    @Test
    internal fun `should create cart item for existing product`() {
        val quantity = 10
        val productId = createProduct(generateProductCreate(quantity = quantity)).id
        val createCartItem = CreateCartItem(productId, faker.random.nextUUID(), quantity)
        val cartCreateJson = """ {
            "productId": "${createCartItem.productId}", 
            "userId": "${createCartItem.userId}", 
            "quantity": ${createCartItem.quantity}
        }""".trimIndent()
        val cartItemJson = cartItemJson(IGNORE_JSON_STRING, createCartItem.productId, createCartItem.quantity)

        RestAssured.given()
            .body(cartCreateJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(CartsController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.CREATED.value())
            .header(HttpHeaders.LOCATION, Matchers.matchesRegex("^/carts/[a-zA-Z0-9-]+$"))
            .body(JsonMatchers.jsonEquals<String>(cartItemJson))
    }

    @Test
    internal fun `should update cart item quantity`() {
        val cartItem = createCartItem(10, 4)
        val updatedQuantity = 3
        val updateCartItemJson = """{
            "quantity": $updatedQuantity
        }""".trimIndent()

        RestAssured.given()
            .body(updateCartItemJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .put(CartsController.ITEM_URI, cartItem.id)
            .then()
            .assertThat()
            .statusCode(HttpStatus.OK.value())
            .body(JsonMatchers.jsonEquals<String>(cartItemJson(cartItem.id, cartItem.productId, updatedQuantity)))
    }

    @Test
    internal fun `should remove cart item with quantity 0`() {
        val cartItem = createCartItem(4, 1)
        val updatedQuantity = 0
        val updateCartItemJson = """{
            "quantity": $updatedQuantity
        }""".trimIndent()

        RestAssured.given()
            .body(updateCartItemJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .put(CartsController.ITEM_URI, cartItem.id)
            .then()
            .assertThat()
            .statusCode(HttpStatus.NO_CONTENT.value())
    }

    private fun cartItemJson(id: String, productId: ProductId, quantity: Int): String =
        """{
            "id": "$id",
            "productId": "$productId", 
            "quantity": $quantity
        }""".trimIndent()
}
