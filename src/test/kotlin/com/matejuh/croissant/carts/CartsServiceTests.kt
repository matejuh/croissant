package com.matejuh.croissant.carts

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.faker
import com.matejuh.croissant.generateProductDto
import com.matejuh.croissant.products.ProductId
import com.matejuh.croissant.products.ProductsRepository
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.kotlin.atMost
import org.awaitility.kotlin.await
import org.awaitility.kotlin.matches
import org.awaitility.kotlin.untilCallTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.repository.findByIdOrNull
import java.time.Duration
import java.time.Instant

class CartsServiceTests : CroissantApplicationTests() {

    @Autowired
    private lateinit var cartsService: CartsService

    @Autowired
    private lateinit var cartsRepository: CartsRepository

    @Autowired
    private lateinit var productsRepository: ProductsRepository

    @Value("\${carts.expiration:30m}")
    private lateinit var cartsExpiration: Duration


    @Test
    internal fun `should create cart when creating cart item and no current cart`() {
        // given
        val userId = faker.random.nextUUID()
        val createCartItem = getCreateCartItem(userId = userId)
        // when
        val notExistingDto = cartsRepository.findByUserId(userId)
        // then
        assertThat(notExistingDto).isNull()
        // when
        cartsService.createCartItem(createCartItem)
        // then
        val cartDto = cartsRepository.findByUserId(userId)
        assertThat(cartDto).isNotNull
    }

    @Test
    internal fun `should use existing cart when creating cart item and there is existing cart`() {
        // given
        val userId = faker.random.nextUUID()
        val createCartItem = getCreateCartItem(userId = userId)
        cartsRepository.save(CartDto(userId))
        // when
        cartsService.createCartItem(createCartItem)
        // then
        val cartDto = cartsRepository.findByUserId(userId)
        assertThat(cartDto).isNotNull
    }

    @Test
    internal fun `should update product quantity when creating cart item`() {
        // given
        val productQuantity = 10
        val cartItemQuantity = 5
        val createCartItem = getCreateCartItem(productQuantity, cartItemQuantity)
        // when
        val cartItem = cartsService.createCartItem(createCartItem)
        // then
        assertThat(getProductQuantity(cartItem.productId)).isEqualTo(productQuantity - cartItemQuantity)
    }

    @Test
    internal fun `should update cart item`() {
        //given
        val productQuantity = 13
        val cartItemQuantity = 8
        val createCartItem = getCreateCartItem(productQuantity, cartItemQuantity)
        val createdCartItem = cartsService.createCartItem(createCartItem)
        val updatedQuantity = 2
        // when
        val updatedCartItem = cartsService.updateCartItem(createdCartItem.id!!, UpdateCartItemQuantity(updatedQuantity))
        // then
        assertThat(updatedCartItem?.quantity).isEqualTo(updatedQuantity)
        assertThat(getProductQuantity(createdCartItem.productId)).isEqualTo(productQuantity - updatedQuantity)
    }

    @Test
    internal fun `should delete cart item when updated to 0`() {
        //given
        val productQuantity = 23
        val createCartItem = getCreateCartItem(productQuantity)
        val createdCartItem = cartsService.createCartItem(createCartItem)
        val updatedQuantity = 0
        // when
        val updatedCartItem = cartsService.updateCartItem(createdCartItem.id!!, UpdateCartItemQuantity(updatedQuantity))
        // then
        assertThat(updatedCartItem).isNull()
        assertThat(getProductQuantity(createdCartItem.productId)).isEqualTo(productQuantity)
    }

    @Test
    internal fun `should evict expired carts`() {
        // given
        repeat(10) {
            cartsRepository.save(
                CartDto(
                    faker.random.nextUUID(),
                    Instant.now().minus(cartsExpiration).minusSeconds(60)
                )
            )
        }
        // when
        cartsService.evictCarts()
        // then
        await atMost Duration.ofSeconds(20) untilCallTo {
            cartsRepository.findByCreatedDateBefore(Instant.now().minus(cartsExpiration))
        } matches { it?.count() == 0 }
    }

    private fun getCreateCartItem(
        productQuantity: Int = faker.random.nextInt(10, 100),
        cartItemQuantity: Int = faker.random.nextInt(1, productQuantity),
        userId: String = faker.random.nextUUID()
    ): CreateCartItem {
        val product = productsRepository.save(generateProductDto(quantity = productQuantity))
        return CreateCartItem(product.id!!, userId, cartItemQuantity)
    }

    private fun getProductQuantity(productId: ProductId) =
        productsRepository.findByIdOrNull(productId)?.quantity
}
