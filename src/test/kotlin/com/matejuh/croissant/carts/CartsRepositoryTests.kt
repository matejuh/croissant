package com.matejuh.croissant.carts

import com.matejuh.croissant.faker
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.persistence.PersistenceException

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CartsRepositoryTests {
    @Autowired
    lateinit var cartsRepository: CartsRepository

    @Autowired
    lateinit var entityManager: TestEntityManager

    @Test
    internal fun `should store cart`() {
        // given
        val cart = CartDto(faker.random.nextUUID())
        // when
        val created = cartsRepository.save(cart)
        // then
        assertThat(created.createdDate).isEqualTo(cart.createdDate)
        assertThat(created.userId).isEqualTo(cart.userId)
        assertThat(created.items).isEmpty()
    }

    @Test
    internal fun `should not be possible to store more carts for same user`() {
        // given
        val userId = faker.random.nextUUID()
        // when
        cartsRepository.save(CartDto(userId))
        val thrown = Assertions.catchThrowable {
            entityManager.persistAndFlush(CartDto(userId))
        }
        // then
        assertThat(thrown).isInstanceOf(PersistenceException::class.java)
    }

    @Test
    internal fun `should delete expired`() {
        // given
        val now = Instant.now()
        val expired = now.minus(5, ChronoUnit.MINUTES)
        repeat(10) {
            cartsRepository.save(CartDto(faker.random.nextUUID(), now.minus(it.toLong(), ChronoUnit.MINUTES)))
        }
        // when
        val removed = cartsRepository.removeExpired(expired)
        // then
        assertThat(removed).isEqualTo(4)
    }
}
