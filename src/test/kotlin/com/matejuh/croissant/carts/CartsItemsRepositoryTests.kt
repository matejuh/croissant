package com.matejuh.croissant.carts

import com.matejuh.croissant.faker
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import javax.transaction.Transactional

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CartsItemsRepositoryTests {
    @Autowired
    private lateinit var cartsItemsRepository: CartsItemsRepository

    @Autowired
    private lateinit var cartsRepository: CartsRepository

    @Test
    internal fun `should create cart item`() {
        // given
        val cart = cartsRepository.save(CartDto(faker.random.nextUUID()))
        val create = CartItemDto(faker.random.nextUUID(), cart, 1)
        // when
        val storedItem = cartsItemsRepository.save(create)
        // then
        assertThat(storedItem).isNotNull
        assertThat(storedItem.id).isNotNull
        assertThat(storedItem.productId).isEqualTo(create.productId)
        assertThat(storedItem.cart.id).isEqualTo(cart.id)
        assertThat(storedItem.quantity).isEqualTo(create.quantity)
    }

    @Test
    internal fun `should update cart item`() {
        // given
        val cart = cartsRepository.save(CartDto(faker.random.nextUUID()))
        val cartItem = cartsItemsRepository.save(CartItemDto(faker.random.nextUUID(), cart, 1))
        val quantity = 5
        // when
        cartItem.quantity = quantity
        val updated = cartsItemsRepository.save(cartItem)
        // then
        assertThat(updated.quantity).isEqualTo(quantity)
    }

    @Test
    @Transactional
    internal fun `should delete all items when cart deleted`() {
        // given
        val cart = cartsRepository.save(CartDto(faker.random.nextUUID()))
        val numberOfItems = 30
        repeat(numberOfItems) {
            cartsItemsRepository.save(CartItemDto(faker.random.nextUUID(), cart, faker.random.nextInt(100)))
        }
        // when
        var cartItems = cartsItemsRepository.countCartItemsByCart(cart)
        // then
        assertThat(cartItems).isEqualTo(numberOfItems.toLong())
        // when
        cartsRepository.delete(cart)
        cartItems = cartsItemsRepository.countCartItemsByCart(cart)
        // then
        assertThat(cartItems).isEqualTo(0L)
    }
}
