package com.matejuh.croissant

import com.matejuh.croissant.carts.CartItem
import com.matejuh.croissant.carts.CartsController
import com.matejuh.croissant.carts.CreateCartItem
import com.matejuh.croissant.orders.UserId
import com.matejuh.croissant.products.Product
import com.matejuh.croissant.products.ProductCreate
import com.matejuh.croissant.products.ProductsController
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.junit.jupiter.api.BeforeEach
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CroissantApplicationTests {

    @LocalServerPort
    var port: Int = 0

    @BeforeEach
    internal fun setUp() {
        RestAssured.port = port
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }

    internal fun createProduct(product: ProductCreate = generateProductCreate()): Product =
        RestAssured.given()
            .body(product)
            .contentType(ContentType.JSON)
            .`when`()
            .post(ProductsController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.CREATED.value())
            .extract().body().`as`(Product::class.java)

    internal fun createCartItem(
        productQuantity: Int,
        cartItemQuantity: Int,
        userId: UserId = faker.random.nextUUID()
    ): CartItem {
        val productId = createProduct(generateProductCreate(quantity = productQuantity)).id
        val createCartItem = CreateCartItem(productId, userId, cartItemQuantity)
        return RestAssured.given()
            .body(createCartItem)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .post(CartsController.BASE_URI)
            .then()
            .statusCode(HttpStatus.CREATED.value())
            .extract()
            .body().`as`(CartItem::class.java)
    }
}
