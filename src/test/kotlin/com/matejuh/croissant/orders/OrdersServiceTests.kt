package com.matejuh.croissant.orders

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.EntityNotFoundException
import com.matejuh.croissant.carts.CartsRepository
import com.matejuh.croissant.carts.CartsService
import com.matejuh.croissant.carts.CreateCartItem
import com.matejuh.croissant.faker
import com.matejuh.croissant.generateProductCreate
import com.matejuh.croissant.products.ProductsService
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class OrdersServiceTests : CroissantApplicationTests() {

    @Autowired
    private lateinit var ordersService: OrdersService

    @Autowired
    private lateinit var productsService: ProductsService

    @Autowired
    private lateinit var cartsService: CartsService

    @Autowired
    private lateinit var cartsRepository: CartsRepository

    @Autowired
    private lateinit var ordersRepository: OrdersRepository

    @Test
    internal fun `should submit order`() {
        // given
        val userId = faker.random.nextUUID()
        val product = productsService.create(generateProductCreate())
        cartsService.createCartItem(CreateCartItem(product.id!!, userId, 1))
        // when
        val order = ordersService.submitOrder(OrderSubmission(userId))
        // then
        assertThat(order.state).isEqualTo(OrderState.CREATED)
        assertThat(cartsRepository.findByUserId(userId)).isNull()
    }

    @Test
    internal fun `should cancel order`() {
        // given
        val userId = faker.random.nextUUID()
        val productQuantity = 4
        val product = productsService.create(generateProductCreate(quantity = 4))
        cartsService.createCartItem(CreateCartItem(product.id!!, userId, 1))
        val order = ordersService.submitOrder(OrderSubmission(userId))
        assertThat(productsService.get(product.id!!)?.quantity).isEqualTo(3)
        // when
        val cancelledOrder = ordersService.cancelOrder(order.id!!)
        // then
        assertThat(cancelledOrder.state).isEqualTo(OrderState.CANCELLED)
        assertThat(productsService.get(product.id!!)?.quantity).isEqualTo(productQuantity)
    }

    @Test
    internal fun `should not cancel order in invalid state`() {
        // given
        val order = ordersRepository.save(OrderDto(faker.random.nextUUID(), OrderState.PAYED))
        // when
        val thrown = Assertions.catchThrowable {
            ordersService.cancelOrder(order.id!!)
        }
        // then
        assertThat(thrown).isInstanceOf(InvalidOrderStateException::class.java)
    }

    @Test
    internal fun `should not cancel not existing order`() {
        // when
        val thrown = Assertions.catchThrowable {
            ordersService.cancelOrder(faker.random.nextUUID())
        }
        // then
        assertThat(thrown).isInstanceOf(EntityNotFoundException::class.java)
    }

    @Test
    internal fun `should confirm payment`() {
        // given
        val order = ordersRepository.save(OrderDto(faker.random.nextUUID(), OrderState.CREATED))
        val paymentId = faker.random.nextUUID()
        // when
        val payedOrder = ordersService.confirmPayment(order.id!!, OrderPayment(paymentId))
        // then
        assertThat(payedOrder.state).isEqualTo(OrderState.PAYED)
        assertThat(payedOrder.paymentId).isEqualTo(paymentId)
    }

    @Test
    internal fun `should not confirm payment of order in invalid state`() {
        // given
        val order = ordersRepository.save(OrderDto(faker.random.nextUUID(), OrderState.CANCELLED))
        // when
        val thrown = Assertions.catchThrowable {
            ordersService.confirmPayment(order.id!!, OrderPayment(faker.random.nextUUID()))
        }
        // then
        assertThat(thrown).isInstanceOf(InvalidOrderStateException::class.java)
    }

    @Test
    internal fun `should not confirm payment of not existing order`() {
        // when
        val thrown = Assertions.catchThrowable {
            ordersService.confirmPayment(faker.random.nextUUID(), OrderPayment(faker.random.nextUUID()))
        }
        // then
        assertThat(thrown).isInstanceOf(EntityNotFoundException::class.java)
    }
}
