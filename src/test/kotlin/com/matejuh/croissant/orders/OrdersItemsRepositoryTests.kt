package com.matejuh.croissant.orders

import com.matejuh.croissant.faker
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class OrdersItemsRepositoryTests {
    @Autowired
    lateinit var ordersItemsRepository: OrdersItemsRepository

    @Autowired
    lateinit var ordersRepository: OrdersRepository

    @Test
    internal fun `should create order item`() {
        // given
        val order = ordersRepository.save(OrderDto(faker.random.nextUUID()))
        val orderItem = OrderItemDto(order, faker.random.nextUUID(), 6)
        // when
        val created = ordersItemsRepository.save(orderItem)
        // then
        assertThat(created.id).isNotNull
        assertThat(created.order.id).isEqualTo(order.id)
        assertThat(created.productId).isEqualTo(orderItem.productId)
        assertThat(created.quantity).isEqualTo(orderItem.quantity)
    }
}
