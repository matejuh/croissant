package com.matejuh.croissant.orders

import com.matejuh.croissant.faker
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class OrdersRepositoryTests {
    @Autowired
    lateinit var ordersRepository: OrdersRepository

    @Test
    internal fun `should create order`() {
        // given
        val order = OrderDto(faker.random.nextUUID())
        // when
        val created = ordersRepository.save(order)
        // then
        assertThat(created.id).isNotNull
        assertThat(created.createdAt).isEqualTo(order.createdAt)
        assertThat(created.state).isEqualTo(OrderState.CREATED)
        assertThat(created.userId).isEqualTo(order.userId)
        assertThat(created.items).isEmpty()
    }

    @Test
    internal fun `should change order state`() {
        // given
        val order = OrderDto(faker.random.nextUUID())
        val created = ordersRepository.save(order)
        // when
        created.state = OrderState.CANCELLED
        val updated = ordersRepository.save(created)
        // then
        assertThat(updated.state).isEqualTo(OrderState.CANCELLED)
    }
}
