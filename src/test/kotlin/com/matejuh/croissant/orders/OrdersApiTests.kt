package com.matejuh.croissant.orders

import com.matejuh.croissant.CroissantApplicationTests
import com.matejuh.croissant.IGNORE_JSON_STRING
import com.matejuh.croissant.faker
import io.restassured.RestAssured
import io.restassured.http.ContentType
import net.javacrumbs.jsonunit.JsonMatchers
import org.hamcrest.Matchers
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus

class OrdersApiTests : CroissantApplicationTests() {
    @Test
    internal fun `should create order`() {
        val userId = faker.random.nextUUID()
        createCartItem(8, 1, userId)
        val orderSubmissionJson = orderSubmissionJson(userId)
        val orderJson = """{"id": "$IGNORE_JSON_STRING"}"""
        RestAssured.given()
            .body(orderSubmissionJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(OrdersController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.CREATED.value())
            .header(HttpHeaders.LOCATION, Matchers.matchesRegex("^/orders/[a-zA-Z0-9-]+$"))
            .body(JsonMatchers.jsonEquals<String>(orderJson))
    }

    @Test
    internal fun `should return error when not existing cart`() {
        val orderSubmissionJson = orderSubmissionJson(faker.random.nextUUID())
        RestAssured.given()
            .body(orderSubmissionJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(OrdersController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
    }

    @Test
    internal fun `should cancel existing order`() {
        val order = createOrder()
        RestAssured.given()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .delete(OrdersController.ITEM_URI, order.id)
            .then()
            .assertThat()
            .statusCode(HttpStatus.NO_CONTENT.value())
    }

    @Test
    internal fun `should return error when cancel not existing order`() {
        RestAssured.given()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .delete(OrdersController.ITEM_URI, faker.random.nextUUID())
            .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
    }

    @Test
    internal fun `should pay existing order`() {
        val order = createOrder()
        val orderPaymentJson = """{"paymentId": "${faker.random.nextUUID()}"}"""
        RestAssured.given()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .body(orderPaymentJson)
            .`when`()
            .put("${OrdersController.ITEM_URI}/payment", order.id)
            .then()
            .assertThat()
            .statusCode(HttpStatus.NO_CONTENT.value())
    }

    @Test
    internal fun `should return error when pay not existing order`() {
        val orderPayment = OrderPayment(faker.random.nextUUID())
        RestAssured.given()
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .body(orderPayment)
            .`when`()
            .put("${OrdersController.ITEM_URI}/payment", faker.random.nextUUID())
            .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
    }

    private fun orderSubmissionJson(userId: UserId) = """{"userId": "$userId"}"""

    private fun createOrder(): Order {
        val userId = faker.random.nextUUID()
        createCartItem(8, 1, userId)
        val orderSubmissionJson = orderSubmissionJson(userId)
        return RestAssured.given()
            .body(orderSubmissionJson)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON)
            .`when`()
            .post(OrdersController.BASE_URI)
            .then()
            .assertThat()
            .statusCode(HttpStatus.CREATED.value())
            .extract()
            .body().`as`(Order::class.java)
    }
}
