FROM openjdk:11.0 as builder

WORKDIR /croissant

ARG JAR_FILE=./build/libs/croissant-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} ./croissant.jar

RUN set -x \
    && mkdir -p dependency \
    && cd dependency \
    && jar -xf ../*.jar \
    && true

FROM openjdk:11.0-jre

LABEL image_name="Croissant app"
LABEL maintainer="Matej Plch <matejuh@gmail.com>"
LABEL git_repository_url="https://github.com/matejuh/croissant"
LABEL parent_image="openjdk:11.0-jre"

RUN groupadd -g 1112 croissant && \
    useradd -r -u 1112 -g croissant croissant
USER croissant

VOLUME /tmp

WORKDIR /croissant

CMD exec java -Djava.security.egd=file:/dev/./urandom $JAVA_OPTS -cp /croissant:/croissant/lib/* \
    com.matejuh.croissant.CroissantApplicationKt

COPY --from=builder /croissant/dependency/BOOT-INF/lib lib
COPY --from=builder /croissant/dependency/META-INF META-INF
COPY --from=builder /croissant/dependency/BOOT-INF/classes .

ARG GIT_COMMIT=unspecified
LABEL git_commit=$GIT_COMMIT
